(ql:quickload :open-license-manager)
;(ql:quickload :clouseau)

(in-package :open-license-manager)

(use-package :plus-c)

;; (clouseau:inspect autowrap::*foreign-types*)

(defun run-it2 ()
  (license-validp "/home/jm/tmp-olm/symbology.lic" "symbology" "111.222.333.444.")
  (license-validp "/home/jm/tmp-olm/symbology/symbology.lic" "symbology" "111.222.333.444.")
  (license-validp "/home/jm/tmp-olm/sbcl.lic" "sbcl" "111.222.333.444.")
  )

(defun run-it ()
  #+NIL
  (license-validp
   "/usr/local/src/licensecc/build/Testing/Temporary/DEFAULT/licenses/expired.lic"
   "DEFAULT"
   "111.222.333.444.")
  (license-validp
   "/usr/local/src/licensecc/build/Testing/Temporary/DEFAULT/licenses/not_expired.lic"
   "DEFAULT"
   "111.222.333.444.")
  #+NIL
  (license-validp
   "/usr/local/src/licensecc/build/Testing/Temporary/DEFAULT/licenses/should-not-be-found.lic"
   "DEFAULT"
   "111.222.333.444."))

#|
(in-package :lib-open-license-manager)

(cl:macroexpand
 (autowrap:c-include
  "/usr/local/src/licensecc/install/include/licensecc/datatypes.h"
  :spec-path '(:open-license-manager :spec)))
|#

;;
;; New version using ASDF/UIOP/IMAGE
;; And able to use command-line-arguments
;;

#|
(ql:quickload :command-line-arguments)

(defparameter +option-spec+
  '((("help" #\h) :type boolean :optional t :documentation "print usage help")))

(defun print-args (args &key port)
  (loop :for arg :in args :do
        (format *terminal-io* " arg = ~a~%" arg)))

(defun run-from-command-line (args &key help)
  (cond (help
         (command-line-arguments:show-option-help +option-spec+))
        (t
	 (format t "(run-from-command-line ~s)~%" args)
	 (run-it)
         ;; Seems like if we don't do this, then we exit straightaway
         #+ccl (ccl::listener-function)
         #+sbcl (sb-impl::toplevel-repl nil)
	 )
	)
  )

#+NIL
(progn 
  (uiop:register-image-restore-hook
   #'(lambda ()
       (command-line-arguments:handle-command-line
        +option-spec+
        #+NIL 'print-args
        #-NIL'run-from-command-line
        :rest-arity t))
   nil)                                 ; Don't call it now

  (uiop:dump-image
   (make-pathname
    :name "foo"
    :type #+ccl "ccl" #+sbcl "sbcl")))
|#
