(in-package :autowrap)

#+NIL
(eval-when (:compile-toplevel :load-toplevel :execute)
  (define-foreign-type 'bool 'uint8)
  (define-foreign-type 'Bool 'uint8)
  (define-foreign-type '_bool 'uint8))

(in-package :lib-open-license-manager)

; (cl:trace AUTOWRAP:DEFINE-FOREIGN-TYPE)a

#+NIL
(eval-when (:compile-toplevel :load-toplevel :execute)
  (cffi:use-foreign-library lib-open-license-manager))

;(defctype bool :bool)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *sys-includes*
    (loop for path in  '("/usr/local/src/licensecc/install/include/licensecc/DEFAULT/"
			 "/usr/local/src/licensecc/install/include/licensecc/"
			 "/usr/lib64/llvm8.0/lib/clang/8.0.0/include/" ; FC30
			 "/usr/lib64/clang/8.0.0/include/"		    ; FC32
			 "/usr/lib64/llvm8.0/include/")
	     when (uiop/filesystem:directory-exists-p path)
       collect (namestring (uiop/filesystem:directory-exists-p path)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (pprint *sys-includes*))

(autowrap:c-include
 "/usr/local/src/licensecc/install/include/licensecc/licensecc.h"
 :sysincludes *sys-includes*
 :spec-path '(:open-license-manager :spec))

#+NIL
(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew #P"/usr/local/src/licensecc/install/lib/licensecc/DEFAULT/" cffi:*foreign-library-directories*)
  (cffi:define-foreign-library liblicensecc-static (t "liblicensecc_static.a"))
  (cffi:define-foreign-library libtools-base (t "libtools_base.a"))
  (cffi:define-foreign-library libbase (t "libbase.a"))
  (cffi:define-foreign-library libos (t "libos.a"))
  (cffi:load-foreign-library 'liblicensecc-static)
  (cffi:load-foreign-library 'libtools-base)
  (cffi:load-foreign-library 'libbase)
  (cffi:load-foreign-library 'libos)
  )

