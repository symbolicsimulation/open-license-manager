;;;; open-license-manager.asd

(asdf:defsystem #:open-license-manager
  :description "Describe open-license-manager here"
  :author "John Morrison <jm@symbolic-simulation.com>"
  :license  "Golden Rule License"
  :version "0.0.1"
;;  :depends-on (:cl-autowrap)
  :depends-on (:cl-plus-c :cl-fad)
  :serial t
  :components ((:module :spec)
	       (:file "package")
	       (:file "lib-open-license-manager")
               (:file "open-license-manager" :depends-on ("lib-open-license-manager"))))
