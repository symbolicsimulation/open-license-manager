;;;; open-license-manager.lisp

(in-package #:open-license-manager)

(use-package :lib-open-license-manager)
(use-package :plus-c)

(defun get-c-string (child-wrapper maxlen)
  (let ((retval (make-array (list maxlen) :adjustable t :fill-pointer t :element-type 'character)))
    (loop for i from 0 to maxlen
;;     until (eq #\NULL (code-char (autowrap:c-aref child-wrapper i :char)))
       until (= 0 (autowrap:c-aref child-wrapper i :char))
       do
	 (let ((hopefully-ascii (autowrap:c-aref child-wrapper i :char)))
	   ;; (format t "[~d]=~x~%" i hopefully-ascii)
	   (cond ((and (>= hopefully-ascii 0) (<= hopefully-ascii 127)) ; Hard-coded to valid ASCII
		  (vector-push-extend (code-char hopefully-ascii) retval))
		 (t (vector-push-extend #\X retval)))))
    retval))

(defun set-c-string (child-wrapper maxlen lisp-string)
  (let ((strlen (min (length lisp-string) maxlen)))
    ;; (format t "child-wrapper ~s~%" child-wrapper)
    ;; (format t "strlen ~s~%" strlen)
    (dotimes (i strlen)
      ;; (format t "00: lisp-string[~d] = ~c~%" i (aref lisp-string i))
      ;; (format t "11: child-wrapper[~d] = ~s~%" i (autowrap:c-aref child-wrapper i :char))
      (setf (autowrap:c-aref child-wrapper i :char) (char-code (aref lisp-string i))))
    (setf (autowrap:c-aref child-wrapper strlen :char) (char-code #\NULL))
    child-wrapper))
  
;;
;; LicenseLocation a.k.a., license-location
;;

(defun print-license-location (ll)
  (format t "PRINT-LICENSE-LOCATION START: --------------------------------~%")
  (format t "license-data-type ~s~%" (license-location.license-data-type ll))
  (let ((child-wrapper (license-location.license-data[]& ll)))
    (format t "license-location ~s~%" (get-c-string child-wrapper +LCC-API-MAX-LICENSE-DATA-LENGTH+)))
  (format t "PRINT-LICENSE-LOCATION END: ----------------------------------~%"))
  
(defun set-license-location (ll data-type data)
  ;; (print "set-license-location~%")			;
  (setf (license-location.license-data-type ll) data-type)
  (let ((child-wrapper (license-location.license-data[]& ll)))
    (set-c-string child-wrapper +LCC-API-MAX-LICENSE-DATA-LENGTH+ data)
    ll))

;;
;; CallerInformations a.k.a., caller-informations
;;

(defun print-caller-information (ci)
  (format t "PRINT-CALLER-INFORMATION START: --------------------------------~%")
  (format t "version ~s~%" (get-c-string (caller-informations.version[]&  ci) +LCC-API-VERSION-LENGTH+))
  (format t "magic ~s~%"  (caller-informations.magic ci))
  (format t "PRINT-CALLER-INFORMATION END: ----------------------------------~%"))

(defun set-caller-information (ci version project-name)
  ;; (print "set-caller-information~%")
  (set-c-string (lib-open-license-manager:caller-informations.version[]& ci) +LCC-API-VERSION-LENGTH+ "111.222.333.444")
  (set-c-string (lib-open-license-manager:caller-informations.feature-name[]& ci) +LCC-API-FEATURE-NAME-SIZE+ "DEFAULT")
  (setf (caller-informations.magic ci) 0) ; Might not be entirely correct
  ci)

;;
;; AuditEvent a.k.a., audit-event
;;

(defun print-audit-event (ai)
  (c-let ((ai audit-event :from ai))
	 (format t "    ~a ~a~%" (severity-string (ai :severity)) (event-type-string (ai :event-type)))
	 (format t "    license_reference: ")
	 (dotimes (i +MAX-PATH+)
	   (format t "~c" (code-char (ai :license-reference i))))
	 (format t "~%    param2: ")
	 (loop for i from 0 below 256
	       until (= 0 (ai :param2 i))
	       do
	       ;; (format t "~x=" (ai :param2 i))
	       (format t "~c" (code-char (ai :param2 i))))
	 (format t "~%")))

;;
;; LicenseInfo a.k.a., license-info
;;

(defun print-license-info (li)
  (format t "PRINT-LICENSE-INFO START: --------------------------------~%")
  (dotimes (i +LCC-API-AUDIT-EVENT-NUM+)
    (format t "status[~d].severity = ~s = ~s~%"
	    i
	    (license-info.status[].severity li i)
	    (severity-string (license-info.status[].severity li i)))
    (format t "status[~d].event-type = ~s~%" i (license-info.status[].severity li i))
    (let ((child-wrapper (license-info.status[].license-reference[]& li i)))
      (format t "status[~d].license-reference = ~s~%" i (get-c-string child-wrapper +MAX-PATH+)))
    (let ((child-wrapper (license-info.status[].param2[]& li i)))
      (format t "status[~d].param2 = ~s~%" i (get-c-string child-wrapper +LCC-API-AUDIT-EVENT-PARAM2+)))
    )
  (let ((child-wrapper (license-info.expiry-date[]& li)))
    (format t "expiry_date: ~s~%" (get-c-string child-wrapper +LCC-API-EXPIRY-DATE-SIZE+)))
  (format t "days_left: ~d~%" (license-info.days-left li))
  (format t "has_expiry: ~d~%" (license-info.has-expiry li))
  (format t "linked_to_pc: ~d~%" (license-info.linked-to-pc li))
  (format t "license_type: ~d~%" (license-info.license-type li))
  (let ((child-wrapper (license-info.proprietary-data[]& li)))
    (format t "propietary_data: ~s~%" (get-c-string child-wrapper +LCC-API-PROPRIETARY-DATA-SIZE+)))
  (format t "license_version: ~d~%" (license-info.license-version li))
  (format t "PRINT-LICENSE-INFO END: ----------------------------------~%"))
  
;;
;; Mapping C++ enums to strings
;;

(defun severity-string (val)
  (cond ((= +SVRT-INFO+ val) "+SVRT-INFO+")
	((= +SVRT-WARN+ val) "+SVRT-WARN+")
	((= +SVRT-ERROR+ val) "+SVRT-ERROR+")
	(t "??????~%")))


(defun event-type-string (retval)
  (cond ((= retval +LICENSE-OK+) "+LICENSE-OK+")
	((= retval +LICENSE-FILE-NOT-FOUND+) "+LICENSE-FILE-NOT-FOUND+")
	((= retval +LICENSE-SERVER-NOT-FOUND+) "+LICENSE-SERVER-NOT-FOUND+")
	((= retval +ENVIRONMENT-VARIABLE-NOT-DEFINED+) "+ENVIRONMENT-VARIABLE-NOT-DEFINED+")
	((= retval +FILE-FORMAT-NOT-RECOGNIZED+) "+FILE-FORMAT-NOT-RECOGNIZED+")
	((= retval +LICENSE-MALFORMED+) "+LICENSE-MALFORMED+")
	((= retval +PRODUCT-NOT-LICENSED+) "+PRODUCT-NOT-LICENSED+")
	((= retval +PRODUCT-EXPIRED+) "+PRODUCT-EXPIRED+")
	((= retval +LICENSE-CORRUPTED+) "+LICENSE-CORRUPTED+")
	((= retval +IDENTIFIERS-MISMATCH+) "+IDENTIFIERS-MISMATCH+")
	((= retval +LICENSE-SPECIFIED+) "+LICENSE-SPECIFIED+")
	((= retval +LICENSE-FOUND+) "+LICENSE-FOUND+")
	((= retval +PRODUCT-FOUND+) "+PRODUCT-FOUND+")
	((= retval +SIGNATURE-VERIFIED+) "+SIGNATURE-VERIFIED+")
	(t
	 (error "oops")
	 #+NIL (clouseau:inspect retval))))

;;
;; Main entry point
;;

#+NIL
(defun find-valid-license-along-path (list-of-directories product-name product-version)
  "return pathname of first valid license type \"lic\" along list of directory pathnames.  Else nil."
  (cond ((null list-of-directories)
	 nil)
	;; Maybe just the first directory is bogus.
	((not (cl-fad:directory-exists-p (first list-of-directories)))
	 (find-valid-license-along-path (rest list-of-directories) product-name product-version))
	(t
	 ;; Try to find a valid license here
	 ;; (format t "dir = ~s~%" (first list-of-directories))
	 (cl-fad:walk-directory
	  (cl-fad:pathname-as-directory (first list-of-directories)) ; No-op if already directory pathname
	  #'(lambda (pname)
	      ;; (format t "  fn ~s~%" pname)
	      (if (open-license-manager::license-validp (namestring pname) product-name product-version)
		  (return-from find-valid-license-along-path pname)
		  nil))
	  :test #'(lambda (pname)
		    ;; (format t "  test ~s~%" pname)
		    (string-equal "lic" (pathname-type pname))))
	 (find-valid-license-along-path (rest list-of-directories) product-name product-version))))

#-NIL
(defun find-valid-license-along-path (list-of-directories product-name product-version)
  "return pathname of first valid license type \"lic\" along list of directory pathnames.  Else nil."
  (dolist (dir list-of-directories)
    (format t "dir ~s~%" dir)
    (when (cl-fad:directory-exists-p dir)
      (format t "dir exists ~s~%" dir)
      (dolist (pname (cl-fad:list-directory dir))
	(format t "   pname ~s~%" pname)
	(when (open-license-manager::license-validp (namestring pname) product-name product-version)
	  (return-from find-valid-license-along-path f)))))
  (format t "Couldn't find license for ~s~%" product-name)
  nil)


(defun license-validp (license-path product-name product-version)
  "returns first value boolean validp and second is OLM event enum"
  ;; (format t "(license-validp license-path = ~s)~%" license-path)
  (autowrap:with-many-alloc ((ll 'license-location)
			     (ci 'caller-informations)
			     (li 'license-info))
    (set-license-location ll +LICENSE-PATH+ license-path)
    ;; (print-license-location ll)
    (set-caller-information ci product-version product-name)
    ;; (print-caller-information ci)
    (let ((retval (acquire-license ci ll li)))
      (when (not (= retval +LICENSE-OK+))
	(format *terminal-io* "NOT +LICENSE-OK+ - file: ~s, result: ~s~%" license-path (event-type-string retval))
	(print-license-location ll)
	(print-caller-information ci)
	(print-license-info li))

	
      (cond ((= retval +LICENSE-OK+)                       (values t retval))
	    ((= retval +LICENSE-FILE-NOT-FOUND+)           (values nil retval))
	    ((= retval +LICENSE-SERVER-NOT-FOUND+)         (values nil retval))
	    ((= retval +ENVIRONMENT-VARIABLE-NOT-DEFINED+) (values nil retval))
	    ((= retval +FILE-FORMAT-NOT-RECOGNIZED+)       (values nil retval))
	    ((= retval +LICENSE-MALFORMED+)                (values nil retval))
	    ((= retval +PRODUCT-NOT-LICENSED+)             (values nil retval))
	    ((= retval +PRODUCT-EXPIRED+)                  (values nil retval))
	    ((= retval +LICENSE-CORRUPTED+)                (values nil retval))
	    ((= retval +IDENTIFIERS-MISMATCH+)             (values nil retval))
	    ((= retval +LICENSE-SPECIFIED+)                (values nil retval))	; ????
	    ((= retval +LICENSE-FOUND+)                    (values nil retval))	; ????
	    ((= retval +PRODUCT-FOUND+)                    (values nil retval))	; ????
	    ((= retval +SIGNATURE-VERIFIED+)               (values nil retval))
	    (t
	     (error "oops")
	     #+NIL (clouseau:inspect retval)))))
#+NIL
  (c-let ((ll license-location :free t)
	  (ci caller-informations :free t)
	  (li license-info :free t))

	 (set-license-location
	  ll
	  +LICENSE-PATH+
	  license-path
	  ;; license-path
	  ;; #+NIL "opt lic dat"
	  ;; #-NIL ""
	  )

	 (set-caller-information
	  ci
	  product-version
	  product-name)

	 (let ((retval (acquire-license ci ll li)))
	   ;; (format t "RETVAL = ~s = ~a~%" retval (event-type-string retval))
	   ;; (print-license-info li)
	   
	   (when (not (= retval +LICENSE-OK+))
	     (print-license-location ll)
	     (print-caller-information ci)
	     )

	   (format *terminal-io* "file: ~s, result: ~s~%" license-path (event-type-string retval))

	   (cond ((= retval +LICENSE-OK+)                       (values t retval))
		 ((= retval +LICENSE-FILE-NOT-FOUND+)           (values nil retval))
		 ((= retval +LICENSE-SERVER-NOT-FOUND+)         (values nil retval))
		 ((= retval +ENVIRONMENT-VARIABLE-NOT-DEFINED+) (values nil retval))
		 ((= retval +FILE-FORMAT-NOT-RECOGNIZED+)       (values nil retval))
		 ((= retval +LICENSE-MALFORMED+)                (values nil retval))
		 ((= retval +PRODUCT-NOT-LICENSED+)             (values nil retval))
		 ((= retval +PRODUCT-EXPIRED+)                  (values nil retval))
		 ((= retval +LICENSE-CORRUPTED+)                (values nil retval))
		 ((= retval +IDENTIFIERS-MISMATCH+)             (values nil retval))
		 ((= retval +LICENSE-SPECIFIED+)                (values nil retval))	; ????
		 ((= retval +LICENSE-FOUND+)                    (values nil retval))	; ????
		 ((= retval +PRODUCT-FOUND+)                    (values nil retval))	; ????
		 ((= retval +SIGNATURE-VERIFIED+)               (values nil retval))
		 (t
		  (error "oops")
		  #+NIL (clouseau:inspect retval))))))	; ????

  

