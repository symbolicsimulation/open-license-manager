;;;; package.lisp

(defpackage #:lib-open-license-manager
  (:use
   #:cffi
   #:plus-c
   #:cl
   )
  (:shadow
   #:abort
   #:abs
   #:random
   )
  )

(defpackage #:open-license-manager
  (:use
   #:cl
   #:cffi
   #:plus-c
   #:lib-open-license-manager)
  (:shadow
   #:abort
   #:abs
   #:random
   )
  (:export
   #:license-validp
   #:find-valid-license-along-path
   )
  )
